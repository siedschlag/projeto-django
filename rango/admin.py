from django.contrib import admin

from rango.models import Post

from rango.models import Category, Page, UserProfile

class CategoryAdmin(admin.ModelAdmin):
     prepopulated_fields = {'slug': ('name',)}
     prepopulated_fields = {'slug': ('name',)}



class PageAdmin(admin.ModelAdmin):
    list_display = ('category', 'title', 'url')
    list_filter = ('category',)
    ordering = ('category', 'title',)


admin.site.register(Category, CategoryAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(UserProfile)
admin.site.register(Post)