from django import forms # importando uma funcao do django

from rango.models.models import Post # importando Post que seria banco.

class PostForm(forms.ModelForm): # uma funcao com o nome PostForm

    class Meta:
        model = Post
        fields = ('title', 'text','author')
        # esta fazendo um formulario com apenas os dois valores, titulo e texto.
        # isso faz com que o usuario escreva algo