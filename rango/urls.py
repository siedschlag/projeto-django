from django.conf.urls import url, include

from . import views
from .views import show_category
from .views import add_blog
from .views import post_list,post_edit,post_new

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^about/$', views.about, name='about'),
    url(r'^add_category', views.add_category, name='add_category'),
    url(r'^category/(?P<category_name_slug>[\w\-]+)/$',


    #url(r'^$',add_blog), # post_list funcao q esta em views
    #url(r'^post/new/$', views.post_new, name='post_new'), #  views.post_new, esta funcao esta views.
    #url(r'^post/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),






    views.show_category, name='show_category'),
    url(r'^category/(?P<category_name_slug>[\w\-]+)/add_page/$',
        views.add_page, name='add_page'),


    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^goto/(?P<page_id>\d+)/$', views.track_url, name='goto'),


    url(r'^$', views.add_blog.post_list), # post_list funcao q esta em views

    url(r'^post/(?P<pk>[0-9]+)/edit/$', views.add_blog.post_edit, name='post_edit'),

    url(r'^post/new/$', views.post_new, name='post_new'),


    #url(r'^post/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),
    #url(r'^post_list/$', views.post_list, name='post_list'),
    #url(r'^post_new/$', views.post_new, name='post_new'),


]
