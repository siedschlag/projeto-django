from .index import index
# from .about import about
# from .show_category import show_category
from .add_category import add_category
from .add_page import add_page
from .register import register
from .user_login import user_login
from .user_logout import user_logout
from .track_url import track_url
from.about import about
from.show_category import show_category
from.add_page import add_page
from.post_edit import post_edit
from.post_list import post_list
from.post_new import post_new