from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404  # sempre necessario para a comunicacao com python e templates
from django.utils import timezone
from rango.models.models import Post # importa Post que seria o banco de dados
from rango.forms.forms import PostForm # importa PostForms que seria
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse


def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('rango.post_detail', pk=post.pk)
            #'blog.views.post_detail'
            #return HttpResponseRedirect(reverse('show_category', kwargs={'category_name_slug': category_name_slug}))
    #return redirect('blog.views.post_detail', pk=post.pk)
            #return HttpResponseRedirect(reverse('index'))
    else:
        form = PostForm(instance=post)
    return render(request, 'rango/post_edit.html', {'form': form})


