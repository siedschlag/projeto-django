
# -*- coding: utf-8 -*-
"""Teste para os formulários dos clientes"""
# python manage.py test
import unittest


class ClientTestCase(unittest.TestCase):
   def setUp(self):
       from rango.forms import UserForm
       cliente1 = {'username': 'Leonardo','password': '123456', 'email':'siedschlag75@gamil.com'}
       self.form1 = UserForm(data=cliente1)

       cliente2 = {'username': 'Leonardo', 'password': '123456', 'email':''}
       self.form2 = UserForm(data=cliente2)

       cliente3 = {'username': 'Leonardo', 'password': '', 'email':'siedschlag75@gmail.com'}
       self.form3 = UserForm(data=cliente3)

       cliente4 = { 'username': '','password': '123456', 'email':'siedschlag75@gmail.com'}
       self.form4 = UserForm(data=cliente4)

       cliente5 = {'username': 'Will', 'password': '1234', 'email':'willian'}
       self.form5 = UserForm(data=cliente5)



       cliente7 = { 'username': 'leo','password': '12345', 'email':''}
       self.form7 = UserForm(data=cliente7)

   def test(self):
       self.assertEquals(self.form1.is_valid(), True)  # Todos os dados
       self.assertEquals(self.form2.is_valid(), True)  # sem email
       self.assertEquals(self.form3.is_valid(), False)  # Sem senha
       self.assertEquals(self.form4.is_valid(), False)  # Sem nome
       self.assertEquals(self.form5.is_valid(), False)  # Sem @.com
       self.assertEquals(self.form7.is_valid(), True)  # sem email
