from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404  # sempre necessario para a comunicacao com python e templates
from django.utils import timezone
from rango.models.models import Post # importa Post que seria o banco de dados
from rango.forms.forms import PostForm # importa PostForms que seria
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse


def post_new(request):
    print("aqui")
    if request.method == "POST":
        print("aqui")
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.published_date = timezone.now()
            post.save()
            return redirect('/', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'rango/post_edit.html', {'form': form})
