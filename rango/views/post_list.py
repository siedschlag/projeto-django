from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404  # sempre necessario para a comunicacao com python e templates
from django.utils import timezone
from rango.models.models import Post # importa Post que seria o banco de dados
from rango.forms.forms import PostForm # importa PostForms que seria
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse



def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date').reverse()
    # esta filtrando, pegando a data e colocando dentro posts.
    return render(request, 'rango/post_list.html',{'posts': posts})
    # esta especificando para colar emblog/post_list.html' e depois retorna o valor para usuario.
