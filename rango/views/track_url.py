from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse

from rango.models.Page import Page


def track_url(request, page_id):
    page = None
    try:
        page = Page.objects.get(id=page_id)
        page.views += 1
        page.save()
    except:
        pass

    if page:
        return HttpResponseRedirect(page.url)
    else:
        return HttpResponseRedirect(reverse('index'))